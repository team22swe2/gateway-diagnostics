# Gateway Diagnostics

## run.py
+ starts socket server on port 8080 & waits for a connection
+ once connection is made, if there's data received, sends JSON to handle_tests()


## diagnostics.py

### handle_tests(json_received)
+ determines which test needs to be run from json_received
+ runs desired test, then packages & returns json object ready to send back to CIC

### run_memory()
+ create a list with one million items
+ Use python exception handling to determine if there is an error
+ Report success or error
+ Error reports shall include exception insights.  

### run_storage()
+ create a one-megabyte file with random characters
+ Use exception handling to determine if there is an error
+ Report success or error to the asset manager
+ Error reports shall include exception insights  

### run_integer()
+ Measures how fast the CPU can perform integer operations
+ An integer is a whole number with no fractional part
+ Use exception handling to determine if there is an error
+ Report success or error to the asset manager
+ Error reports shall include exception insights  

### run_prime()
+ Measures how fast the CPU can search for prime numbers
+ example, 1, 2, 3, 5, 7, 11, etc. This algorithm should use a loop
+ Report success or error to the asset manager
+ Error reports shall include exception insights. 

### run_floating()
+ Performs the same operations as the integer math tests, however with floating point numbers
+ A floating point number is a number with fractional parts (e.g., 10.12345)
+ Report success or error to the asset manager 
+ Error reports shall include exception insights