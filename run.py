# run.py Style Guide
#
#----- How process using sockets works -------
# This Python Server is started first, waiting for a connection on port 8080.
# Start Java client second, connection on port 8080 should be found, by utilizing socket library
#
# Overall, this server runs forever listening for connections & printing data received to console
#
#----- Still need to create a .py file that will include all the diagnostic test


import socket
import json
from datetime import datetime
from diagnostics import start_tests, handle_tests

HOST = "localhost"
PORT = 8080
TIME_INTERVAL = 5

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)


# Bind the socket to the port
server_address = ('localhost', 8080)
print('starting up on {} port {}'.format(*server_address))
sock.bind(server_address)

# Listen for incoming connections
sock.listen(1)


while True:
    # Wait for a connection
    print('waiting for a connection')
    connection, client_address = sock.accept()
    try:
        print('connection from', client_address)

        # Receive the data & print to console. In the future will need to run tests based on data
        while True:
            data = connection.recv(1024)
            data_received = ""

            if data:
                data_received = json.loads(data.decode())

            if data_received:
                print("Received: ")
                print(data_received)

                test_array = []
                gw_id=""

                try:
                    test_array = data_received["test_queue"]
                    gw_id = data_received["gwID"]
                except Exception as e:
                    print(e)

                results = handle_tests(test_array, gw_id)
                connection.sendall(results.encode())
            else:
                print('no data from', client_address)
                break

    finally:
        # Clean up the connection
        connection.close()

