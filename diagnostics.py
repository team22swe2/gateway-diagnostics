from datetime import datetime
import json
import random
import string

def handle_tests(array, gw_id):
    test_results = []

    # if len array is 0
    if (len(array) > 0) :
        for test in array:
            print ("Running: " + test + "..")
            test_results.append(start_tests(test, gw_id))
    else:
        test_results.append(build_result(gw_id,"","Fail",None, None, None, "Empty array of tests"))

    return json.dumps({"test_results": test_results}) + "\n"

def start_tests(test_type, gw_id):

    # iso format turns time into string so it can be json.dumped & shipped
    start = datetime.now()
    timestamp_start = start.isoformat()

    error_report = ''
    test_result = "Pass"

    # determine which test to run
    if (test_type == "memory_test"):
        error_report = run_memory()
    elif(test_type == "floating_test"):
        error_report = run_floating()
    elif(test_type == "storage_test"):
        error_report = run_storage()
    elif(test_type == "integer_test"):
        error_report = run_integer()
    elif(test_type == "prime_test"):
        error_report = run_prime()

    # Record test as Failed if there's an error
    if (error_report != "None" or gw_id == ""):
        test_result = "Fail"

    end = datetime.now()
    timestamp_end = end.isoformat()
    elapsed_time = str(end - start)

    return json.dumps(build_result(gw_id, test_type, test_result, timestamp_start, timestamp_end, elapsed_time, error_report))

# takes in a bunch of parameters to build a dictionary
# 1 dictionary per test ran
def build_result(gw_id, test_type, test_result, timestamp_start, timestamp_end, elapsed_time, error_report):
    result = {"gwID": str(gw_id), "testType": str(test_type), "testResult": test_result, "timestampStart": str(timestamp_start),
              "timestampEnd": str(timestamp_end), "elapsedTime": str(elapsed_time), "errorReport": error_report}
    return result


# create a list with one million items.
# Use python exception handling to determine if there is an error. Report success or error
def run_memory():
    error_report = "None"
    try :
        list = []
        for i in range(0, 1000000):
            list.append(random.randint(1, 10))
    except MemoryError as error:
        error_report = str(error)
    except Exception as exception:
        error_report = str(exception)

    return error_report

# Performs the same operations as the integer math tests, however with floating point numbers.
# A floating point number is a number with fractional parts (e.g., 10.12345)
# Use python exception handling to determine if there is an error. Report success or error
def run_floating():
    error_report = "None"
    try:
        random_float_1 = random.uniform(1, 100)
        random_float_2 = random.uniform(1, 100)
        print(random_float_1)
        print(random_float_2)

        for i in range(0, 1000000):
            # generate random number to determine which integer operation to user
            random_operation = random.randint(1, 4)
            if (random_operation == 1):
                result = random_float_1 + random_float_2
            elif (random_operation == 2):
                result = random_float_1 - random_float_2
            elif (random_operation == 3):
                result = random_float_1 * random_float_2
            elif (random_operation == 4):
                result = random_float_1 / random_float_2

    except Exception as exception:
        error_report = str(exception)

    return error_report


# create a one-megabyte file with random characters.
# Use exception handling to determine if there is an error.
def run_storage():
    error_report = "None"
    try:
        file = open("storageTest","w")
        for i in range(0,1000000):
            file.write(random.choice(string.ascii_letters))

        # print(file.tell())
        file.close()
    except Exception as exception:
        error_report = str(exception)

    return error_report

#  Measures how fast the CPU can perform integer operations.
#  An integer is a whole number with no fractional part.
#  Use exception handling to determine if there is an error
def run_integer():
    error_report = "None"
    try :
        random_int_1 = random.randint(1, 100)
        random_int_2 = random.randint(1, 100)

        for i in range(0, 1000000):
            # generate random number to determine which integer operation to user
            random_operation = random.randint(1,4)
            if (random_operation == 1):
                result = random_int_1 + random_int_2
            elif (random_operation == 2):
                result =random_int_1 - random_int_2
            elif (random_operation == 3):
                result = random_int_1 * random_int_2
            elif (random_operation == 4):
                result = random_int_1 / random_int_2

    except Exception as exception:
        error_report = str(exception)

    return error_report

# Measures how fast the CPU can search for prime numbers. example, 1, 2, 3, 5, 7, 11, etc.
# This algorithm should use a loop. Report success or error to the asset manager.
# Error reports shall include exception insights.
def run_prime():
    error_report = "None"
    prime_count = 0
    try:
        array = get_random_array(1000000)
        for i in array:
            if (i % 2 == 1):
                prime_count = prime_count +1
    except Exception as exception:
        error_report = str(exception)

    print(str(prime_count) + " prime numbers found")
    return error_report

def get_random_array(length):
    array = []
    for i in range(0, length):
        array.append(random.randint(1,200))

    return array